package com.miday.s3Activity;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        var factorial = 1;
        var num = 1;
        System.out.println("Enter any integer.");

        try {
            num = input.nextInt();
            if (num < 0){
                System.out.println("Negative is not factorial.");
            } else if (num >= 1) {
                for (var i = 1; i <= num; i++ ) {
                    factorial = factorial * i;
                }
                System.out.println(String.format("Factorial of %d is " + factorial + ".", num));
            } else if (num == 0) {
                System.out.println("Factorial of 0 is 1.");
            }
        } catch (Exception err) {
            System.out.println("Invalid input.");
        }



    }
}
